#!/usr/bin/env python2
import refpy2

def foo():
    x = 5
    @refpy2.withref
    def bar():
        y = ref(x)
        y += 3
    print x
    bar()
    print x
    bar()
    print x
    return x

foo()
