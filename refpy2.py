import dis
import types

class RefError(RuntimeError):
    pass

TOP = 0
GOT_REF = 1
GOT_DEREF = 2
GOT_CALL = 3

def withref(fn):
    fn_co = fn.__code__
    bytecode = fn_co.co_code
    i = 0
    n = len(bytecode)
    om = dis.opmap

    state = 0
    mappings = {}
    deref = None

    out = ""

    while i<n:
        op = ord(bytecode[i])
        i += 1
        oarg = None
        if op >= dis.HAVE_ARGUMENT:
            oarg = ord(bytecode[i]) | (ord(bytecode[i+1])<<8)
            i+=2
        if state == TOP:
            if op == om["LOAD_GLOBAL"] and fn_co.co_names[oarg] == "ref":
                state = GOT_REF
                op = om["NOP"]
            elif op == om["LOAD_FAST"] and oarg in mappings:
                op = om["LOAD_DEREF"]
                oarg = mappings[oarg]
            elif op == om["STORE_FAST"] and oarg in mappings:
                op = om["STORE_DEREF"]
                oarg = mappings[oarg]
            elif op == om["DELETE_FAST"] and oarg in mappings:
                op = om["DELETE_DEREF"]
                oarg = mappings[oarg]
        elif state == GOT_REF:
            if op == om["LOAD_DEREF"]:
                state = GOT_DEREF
                deref = oarg
                op = om["NOP"]
            else:
                raise RefError("Invalid usage of ref", state, op)
        elif state == GOT_DEREF:
            if op == om["CALL_FUNCTION"]:
                state = GOT_CALL
                op = om["NOP"]
            else:
                raise RefError("Invalid usage of ref", state, op)
        elif state == GOT_CALL:
            if op == om["STORE_FAST"]:
                state = TOP
                op = om["NOP"]
                mappings[oarg] = deref
                deref = None
            else:
                raise RefError("Invalid usage of ref", state, op)
        out += chr(op)
        if op >= dis.HAVE_ARGUMENT:
            out += chr(oarg & 255)
            out += chr((oarg>>8) & 255)

    n_fn_co = types.CodeType(
            fn_co.co_argcount,
            fn_co.co_nlocals,
            fn_co.co_stacksize,
            fn_co.co_flags,
            out,
            fn_co.co_consts,
            fn_co.co_names,
            fn_co.co_varnames,
            fn_co.co_filename,
            fn_co.co_name,
            fn_co.co_firstlineno,
            fn_co.co_lnotab,
            fn_co.co_freevars,
            fn_co.co_cellvars)
    return types.FunctionType(
            n_fn_co,
            fn.__globals__,
            fn.__name__,
            fn.__defaults__,
            fn.__closure__)
